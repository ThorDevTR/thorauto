TXT="ThorUserBot Otomatik Deploy Kuruluma Hoş Geldiniz"
TXT+="\nTelegram: @ThorUserBot"
pkg update -y
rm -rf ThorInstaller
clear
echo -e $TXT
echo "Python Yükleniyor ⌛"
pkg install python -y
clear
echo -e $TXT
echo "Git Yükleniyor ⌛"
pkg install git -y
clear
echo -e $TXT
echo "Pyrogram Yükleniyor ⌛"
pip install pyrogram tgcrypto
echo "Repo klonlanıyor... ⌛"
git clone https://github.com/ThorDevTR/ThorInstaller
clear
echo -e $TXT
cd ThorInstaller
clear
echo "Gereksinimler Yükleniyor ⌛"
echo -e $TXT
pip install -r requirements.txt
clear
python -m thor_installer
